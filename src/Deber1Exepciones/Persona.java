package Deber1Exepciones;
//Clase Persona
public abstract class Persona {
    /*Atributos*/
    private String nombre;
    private char sexo;
    private int edad;
    private boolean asistencia;
    /*Contantes*/
    private final String[] NOMBRES_CHICOS={"Jose", "Alex", "Jaime", "Victor", "Carlos"}; 
    private final String[] NOMBRES_CHICAS={"Janeth", "Glenda", "Maria", "Belen", "Nicole"}; 
    private final int CHICO=0;
    private final int CHICA=1;
    public Persona(){   
        int SexoPersona = NumeroAleatorios.Aleatorios(0,1);
        if(SexoPersona == CHICO){
            nombre = NOMBRES_CHICOS[NumeroAleatorios.Aleatorios(0,4)];
            sexo='H';
        }else{
            nombre=NOMBRES_CHICAS[NumeroAleatorios.Aleatorios(0,4)];
            sexo='M';
        }     
        novillo(); 
    }
    public String getNombre() {
        return nombre;
    } 
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public char getSexo() {
        return sexo;
    } 
    public void setSexo(char sexo) {
        this.sexo = sexo;
    }
    public int getEdad() {
        return edad;
    }
    public void setEdad(int edad) {
        this.edad = edad;
    }
    public boolean isAsistencia() {
        return asistencia;
    }
    public void setAsistencia(boolean asistencia) {
        this.asistencia = asistencia;
    }  
    public abstract void novillo();    
}

class Alumno extends Persona{
    private int nota;   
    public Alumno(){
        super();   
        nota = NumeroAleatorios.Aleatorios(0,10);  
        super.setEdad(NumeroAleatorios.Aleatorios(12,15));        
    }   
    public int getNota() {
        return nota;
    }
    public void setNota(int nota) {
        this.nota = nota;
    }
    @Override
    public void novillo() {
        int AsistenciaAlumno = NumeroAleatorios.Aleatorios(0, 100);
        if(AsistenciaAlumno < 50){
            super.setAsistencia(false);
        }else{
            super.setAsistencia(true);
        }       
    }  
    @Override
    public String toString(){      
        return "Nombre: "+super.getNombre()+"\tsexo: "+super.getSexo()+"\tedad: "+super.getEdad()+ "\tnota: "+nota;   
    } 
}

class Profesor extends Persona{  
    private String materia; 
    public Profesor(){
       super();     
       super.setEdad(NumeroAleatorios.Aleatorios(25,50));    
       materia=Materias.MATERIAS[NumeroAleatorios.Aleatorios(0,2)];
    }
    public String getMateria() {
        return materia;
    }
    public void setMateria(String materia) {
        this.materia = materia;
    }
    @Override
    public void novillo() {     
        int AsistenciProfesor=NumeroAleatorios.Aleatorios(0, 100);  
        if(AsistenciProfesor<20){
            super.setAsistencia(false);
        }else{
            super.setAsistencia(true);
        }       
    }
}
class Aula1 { 
    /*Atributos*/
    private final int id;
    private final Profesor profesor;
    private final Alumno[] alumnos;
    private final String materia;   
    private final int MAX_ALUMNOS=20; 
    public Aula1(){      
        id=1;        
        profesor=new Profesor();
        alumnos= new Alumno[MAX_ALUMNOS];
        creaAlumnos();
        materia=Materias.MATERIAS[NumeroAleatorios.Aleatorios(0,2)];     
    }    
    private void creaAlumnos(){ 
        for(int i=0;i<alumnos.length;i++){
            alumnos[i]=new Alumno();
        }    
    }     
    private boolean asistenciaAlumnos(){
        int cuentaAsistencias=0;
        //contamos las asistencias
        for(int i=0;i<alumnos.length;i++){
            if(alumnos[i].isAsistencia()){
                cuentaAsistencias++;
            }
        }
        System.out.println("Hay "+cuentaAsistencias+" alumnos");
        return cuentaAsistencias>=((int)(alumnos.length/2));   
    } 
    public boolean darClase(){
        try {
         if(!profesor.isAsistencia()){
            System.out.println("El profesor no esta, no se puede dar clase");
            return false;
        }else if(!profesor.getMateria().equals(materia)){
            System.out.println("La materia del profesor y del aula no es la misma, no se puede dar clase");
            return false;
        }else if (!asistenciaAlumnos()){
            System.out.println("La asistencia no es suficiente, no se puede dar clase");
            return false;
        }   
        } catch (Exception e) {
        }
         System.out.println("Se puede dar clase");
        return true;
    }
    public void notas(){
        int chicosApro=0;
        int chicasApro=0;
         for(int i=0;i<alumnos.length;i++){    
           if(alumnos[i].getNota()>=5){
               if(alumnos[i].getSexo()=='H'){
                   chicosApro++;
               }else{
                   chicasApro++;
               } 
               System.out.println(alumnos[i].toString());  
           }  
        }  
        System.out.println("Hay "+chicosApro+" chicos y "+chicasApro+" chicas aprobados/as");
    }  
}
class Materias {
    public static final String[] MATERIAS={"Matematicas", "Filosofia", "Fisica"};   
}
class NumeroAleatorios {
    public static int Aleatorios(int minimo, int maximo){
        int num=(int)Math.floor(Math.random()*(minimo-(maximo+1))+(maximo+1));
        return num;
    }
}
class Principal {
    public static void main(String[] args) {
        //Creamos el objeto
        Aula1 aula=new Aula1();
        //Indicamos si se puede dar la clase
        if(aula.darClase()){
            aula.notas();
        }  
    }   
}